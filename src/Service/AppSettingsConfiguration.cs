﻿using Domain.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Service
{
    public class AppSettingsConfiguration : IAppSettingsConfiguration
    {
        private readonly IConfiguration _config;

        public AppSettingsConfiguration(IConfiguration config)
        {
            _config = config;
        }

        public int LimiteTijolosHorizontal()
        {
            return ObterValor("TotalTijolosHorizontal");
        }

        public int LimiteTijolosVertical()
        {
            return ObterValor("TotalTijolosVertical");
        }

        public int ObterTotalMaximoTijolos()
        {
            return ObterValor("TotalMaximoTijolos");
        }

        public long SomaMaximaTotalDeCadaLinhaDeTijolos()
        {
            int valor = ObterValor("SomaMaximaTotalDeCadaLinhaDeTijolos");

            return valor == 0 ? int.MaxValue : valor;
        }

        private int ObterValor(string parametro)
        {
            string valor = _config.GetSection(parametro).Value;

            return int.TryParse(valor, out int retorno) ? retorno : 0;
        }
    }
}