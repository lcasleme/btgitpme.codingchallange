﻿using Domain.Interfaces;
using Domain.Models;
using System;

namespace Service
{
    public class BigOService : IBigOService
    {
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;

        public BigOService(IAppSettingsConfiguration appSettingsConfiguration)
        {
            _appSettingsConfiguration = appSettingsConfiguration;
        }

        public string RealizarAnalise(Parede parede)
        {
            double ms = (parede.TempoTotalExecucao.TotalMilliseconds / (parede.TotalLinhas * parede.TotalTijolosLinha));
            if (parede.TotalLinhas == parede.TotalTijolosLinha && parede.TotalLinhas == LimiteMaximoDeExecucoes())
                return On2(parede.TotalLinhas, ms);
            else
                return Onm(parede.TotalLinhas, parede.TotalTijolosLinha, ms);
        }

        private string Onm(int n, int m, double msUnitario)
        {
            int max = n * m;
            return $"O(nm) Sendo n = {n} e m = {m} - Será executado no máximo: {max} e poderá levar um tempo médio de: {TempoMaximo(max, msUnitario)}";
        }

        private string On2(int n, double msUnitario)
        {
            int max = (int)Math.Round(Math.Pow(n, n), MidpointRounding.AwayFromZero);
            return $"O(n²) Sendo n = {n} - Este é modelo mais crítico pois será executado n² sendo o máximo possível de execuções dentro dos límites, máximo: {max} e poderá levar um tempo médio de: {TempoMaximo(max, msUnitario)}";
        }

        private int LimiteMaximoDeExecucoes()
        {
            return (int)Math.Round(Math.Sqrt(_appSettingsConfiguration.ObterTotalMaximoTijolos()), MidpointRounding.AwayFromZero);
        }

        private string TempoMaximo(int max, double ms)
        {
            double TotalMilliseconds = ms * max;
            return $"{TotalMilliseconds} ms";
        }
    }
}