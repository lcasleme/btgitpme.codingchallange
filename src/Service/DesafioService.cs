﻿using Domain.Interfaces;
using Domain.Models;
using System;

namespace Service
{
    public class DesafioService : IDesafioService
    {
        private readonly IParedeService _paredeService;
        private readonly IBigOService _bigOService;

        public DesafioService(IParedeService paredeService, IBigOService bigOService)
        {
            _paredeService = paredeService;
            _bigOService = bigOService;
        }

        public string ObterResultado()
        {
            Parede parede = _paredeService.ObterParede();

            MenorColunaTotalTijolosCortados menorColunaTotalTijolosCortados = ObterMenorCaminhoEQuantidadeTijolos(parede);
            string textoRetorno = ObterTextoRetorno(parede, menorColunaTotalTijolosCortados.MenorColuna);
            string retornoBigO = _bigOService.RealizarAnalise(parede);
            string texto = $@"Total de linhas na parede: {parede.TotalLinhas}, e um total de {parede.TotalTijolosLinha} tijolos em cada linha.{System.Environment.NewLine}
O menor caminho é: {menorColunaTotalTijolosCortados.MenorColuna}, cortando um total de: {menorColunaTotalTijolosCortados.TotalTijoloCortado} tijolos.{System.Environment.NewLine}
{retornoBigO}{System.Environment.NewLine}{System.Environment.NewLine}{textoRetorno}";

            return texto;
        }

        private MenorColunaTotalTijolosCortados ObterMenorCaminhoEQuantidadeTijolos(Parede parede)
        {
            Int64 menorTamanhoDeTijolosCortados = Int64.MaxValue;
            int menorColuna = 0;
            int totalTijolosCortados = 0;
            for (int i = 0; i < parede.TotalTijolosLinha; i++)
            {
                Int64 totalColuna = 0;
                int quantidadeTijolosCortados = 0;
                for (int y = 0; y < parede.TotalLinhas; y++)
                {
                    Int64 tamanho = parede.Linhas[y].Tijolo[i].Tamanho;
                    totalColuna += tamanho;
                    if (tamanho > 0)
                        quantidadeTijolosCortados += 1;
                }

                if (menorTamanhoDeTijolosCortados > totalColuna)
                {
                    menorTamanhoDeTijolosCortados = totalColuna;
                    menorColuna = i;
                    totalTijolosCortados = quantidadeTijolosCortados;
                }
            }

            return new MenorColunaTotalTijolosCortados(menorColuna, totalTijolosCortados);
        }

        private string ObterTextoRetorno(Parede parede, int menorColuna)
        {
            string textoRetorno = "";

            for (int i = 0; i < parede.Linhas.Length; i++)
            {
                string linha = "";

                for (int y = 0; y < parede.Linhas[i].Tijolo.Length; y++)
                {
                    if (y == menorColuna)
                        linha += $"|{parede.Linhas[i].Tijolo[y].Tamanho:D10}| ";
                    else
                        linha += $"{parede.Linhas[i].Tijolo[y].Tamanho:D10} ";
                }

                linha += System.Environment.NewLine;

                textoRetorno += linha;
            }

            return textoRetorno;
        }
    }
}