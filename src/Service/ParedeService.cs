﻿using Domain.Interfaces;
using Domain.Models;
using System;
using System.Diagnostics;

namespace Service
{
    public class ParedeService : IParedeService
    {
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;

        public ParedeService(IAppSettingsConfiguration appSettingsConfiguration)
        {
            _appSettingsConfiguration = appSettingsConfiguration;
        }

        public Parede ObterParede()
        {
            Stopwatch timer = Stopwatch.StartNew();

            int linhas = new Random().Next(1, _appSettingsConfiguration.LimiteTijolosHorizontal());
            int tijolos = new Random().Next(1, _appSettingsConfiguration.LimiteTijolosVertical());

            if ((linhas * tijolos) > _appSettingsConfiguration.ObterTotalMaximoTijolos())
                return ObterParede();

            Parede parede = new Parede
            {
                Linhas = new Linha[linhas],
                TotalLinhas = linhas,
                TotalTijolosLinha = tijolos
            };

            for (int linha = 0; linha < linhas; linha++)
            {
                parede.Linhas[linha] = new Linha()
                {
                    Tijolo = new Tijolo[tijolos]
                };

                Int64 tamanhoDaLinha = 0;

                for (int tijolo = 0; tijolo < tijolos; tijolo++)
                {
                    TamanhoTijoloLinha tamanhoLinhaTamanhoTijolo = ObterTamanhoDosTijolosDaLinha(tamanhoDaLinha, parede, tijolo == (tijolos - 1));

                    tamanhoDaLinha = tamanhoLinhaTamanhoTijolo.NovoTamanhoDaLinha;
                    parede.Linhas[linha].Tijolo[tijolo] = new Tijolo()
                    {
                        Tamanho = (int)tamanhoLinhaTamanhoTijolo.TamanhoTijolo
                    };
                }
            }

            timer.Stop();
            parede.TempoTotalExecucao = timer.Elapsed;

            return parede;
        }

        private TamanhoTijoloLinha ObterTamanhoDosTijolosDaLinha(Int64 tamanhoDaLinha, Parede parede, bool ultimoTijolo)
        {
            if (tamanhoDaLinha == _appSettingsConfiguration.SomaMaximaTotalDeCadaLinhaDeTijolos())
                return new TamanhoTijoloLinha() { NovoTamanhoDaLinha = tamanhoDaLinha, TamanhoTijolo = 0 };

            Int64 tamanhoTijolo = new Random().Next(1, (int)(_appSettingsConfiguration.SomaMaximaTotalDeCadaLinhaDeTijolos() - tamanhoDaLinha));

            if (ultimoTijolo)
                tamanhoTijolo = _appSettingsConfiguration.SomaMaximaTotalDeCadaLinhaDeTijolos() - tamanhoDaLinha;

            Int64 novoTamanhoDaLinha = tamanhoDaLinha + tamanhoTijolo;

            if (novoTamanhoDaLinha > _appSettingsConfiguration.SomaMaximaTotalDeCadaLinhaDeTijolos())
                return ObterTamanhoDosTijolosDaLinha(tamanhoDaLinha, parede, ultimoTijolo);

            return new TamanhoTijoloLinha() { NovoTamanhoDaLinha = novoTamanhoDaLinha, TamanhoTijolo = tamanhoTijolo };
        }
    }
}