﻿using Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DesafioController : ControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get([FromServices] IDesafioService desafioService)
        {
            return Ok(desafioService.ObterResultado());
        }
    }
}