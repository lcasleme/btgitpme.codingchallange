﻿namespace Domain.Models
{
    public class MenorColunaTotalTijolosCortados
    {
        public MenorColunaTotalTijolosCortados(int menorColuna, int totalTijoloCortado)
        {
            MenorColuna = menorColuna;
            TotalTijoloCortado = totalTijoloCortado;
        }

        public int MenorColuna { get; private set; }
        public int TotalTijoloCortado { get; private set; }
    }
}