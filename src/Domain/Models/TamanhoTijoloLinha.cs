﻿using System;

namespace Domain.Models
{
    public class TamanhoTijoloLinha
    {
        public Int64 NovoTamanhoDaLinha { get; set; }
        public Int64 TamanhoTijolo { get; set; }
    }
}