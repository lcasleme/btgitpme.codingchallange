﻿using System;

namespace Domain.Models
{
    public class Parede
    {
        public Linha[] Linhas { get; set; }
        public int TotalLinhas { get; set; }
        public int TotalTijolosLinha { get; set; }

        public TimeSpan TempoTotalExecucao { get; set; }
    }
}