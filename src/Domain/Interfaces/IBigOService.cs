﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IBigOService
    {
        string RealizarAnalise(Parede parede);
    }
}