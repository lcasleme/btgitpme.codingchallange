﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IParedeService
    {
        Parede ObterParede();
    }
}