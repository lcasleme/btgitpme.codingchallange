﻿namespace Domain.Interfaces
{
    public interface IDesafioService
    {
        string ObterResultado();
    }
}