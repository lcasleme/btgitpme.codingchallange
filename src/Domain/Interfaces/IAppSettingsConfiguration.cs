﻿using System;

namespace Domain.Interfaces
{
    public interface IAppSettingsConfiguration
    {
        int ObterTotalMaximoTijolos();

        int LimiteTijolosVertical();

        int LimiteTijolosHorizontal();

        Int64 SomaMaximaTotalDeCadaLinhaDeTijolos();
    }
}